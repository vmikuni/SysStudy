#!/bin/bash 
#Simple script to copy the data for test and application
if [ ! -d "data" ] 
then
    echo "Creating data folder"
    mkdir data
fi
echo "Copying the data..." 
hadd -f data/TestEvents.root /work/vmikuni/data_sys/nominal/SelectedEvents_2*
hadd -f data/AllEvents.root /work/vmikuni/data_sys/nominal/SelectedEvents_*
