#!/bin/bash
#Run on all relevant options for Plot_postfit.py
FOLDER=$1
CURRDIR="$PWD"
for f in $FOLDER/*; do
    if [ -d "$f" ]; then
	if [[ $f == *"regions"* ]]; 
	then
	    echo "Calling Plot_postfit.py -d $(basename $f) -c highsys"
	    python Plot_postfit.py -d $(basename $f) -c highsys
	    python Plot_postfit.py -d $(basename $f) -c highsys --pref
	    #python Plot_postfit.py -d $(basename $f) -c highsys --pull
	    #python Plot_postfit.py -d $(basename $f) -c highsys --pref --pull
	    echo "Calling Plot_postfit.py -d $(basename $f) -c lowsys"
	    python Plot_postfit.py -d $(basename $f) -c lowsys
	    python Plot_postfit.py -d $(basename $f) -c lowsys --pref
	    #python Plot_postfit.py -d $(basename $f) -c lowsys --pull
	    #python Plot_postfit.py -d $(basename $f) -c lowsys --pref --pull
	else
	    echo "Calling Plot_postfit.py -d $(basename $f) -c merged"
	    python Plot_postfit.py -d $(basename $f) -c merged
	    python Plot_postfit.py -d $(basename $f) -c merged --pref
	    #python Plot_postfit.py -d $(basename $f) -c merged --pull
	    #python Plot_postfit.py -d $(basename $f) -c merged --pref --pull
	fi
	
	#cd $CURRDIR/$f
	#./do_fit.sh		
	#cd $CURRDIR
    fi
done





