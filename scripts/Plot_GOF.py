import os,sys
import ROOT as rt
import  tdrstyle
import CMS_lumi
from Plotting_cfg import *
import numpy as np
from math import *
from optparse import OptionParser
rt.PyConfig.IgnoreCommandLineOptions = True

rt.gROOT.SetBatch(True)
rt.gStyle.SetOptStat(0)
tdrstyle.setTDRStyle()


    
    

parser = OptionParser(usage="%prog [options]")
parser.add_option("-p","--plots",dest="plots", type="string", default="../Plots", help="Path to store plots. [default: %default]")
parser.add_option("-f","--folder",dest="folder", type="string", default="../datacards", help="Base path for the folder with results. [default: %default]")
parser.add_option("-d","--dir",dest="dir", type="string", default="mjjj_bkgfrac0.2_Test_regions", help="Directory of the folder with the GOF results. The directory is relative to the folder set by -f.  [default: %default]")

parser.add_option("-s","--save",dest="save", action="store_true", default=True, help="Save the plots. [default: %default]")
parser.add_option("-m","--mode",dest="mode",type="string", default="saturated", help="GOF to plot. Options are [saturated/KS/AD/CH]. [default: %default]")
parser.add_option("-b","--bins",dest="bins",type="int", default=30, help="Number of bins to use. [default: %default]")
parser.add_option("--seed",dest="seed",type="int", default=1234, help="Seed used for toy generation. [default: %default]")


(options, args) = parser.parse_args()

toy_file = "higgsCombine{}_1000.GoodnessOfFit.mH120.{}.root".format(options.mode,options.seed)
single_file = "higgsCombine{}.GoodnessOfFit.mH120.root".format(options.mode)
if options.mode == "CH":
    toy_file = "higgsCombine{}_1000.ChannelCompatibilityCheck.mH120.{}.root".format(options.mode,options.seed)
    single_file = "higgsCombine{}.ChannelCompatibilityCheck.mH120.root".format(options.mode)


tree_name ='limit'
single_root = rt.TFile.Open(os.path.join(options.folder,options.dir,single_file),"READ")
single_tree=single_root.Get(tree_name)
toy_root = rt.TFile.Open(os.path.join(options.folder,options.dir,toy_file),"READ")
toy_tree=toy_root.Get(tree_name)

for e,event in enumerate(single_tree):
    single_limit=event.limit

toys = [] #At the moment, I will fill an array first, since I don't know the optimal bounds for the histogram
for e,event in enumerate(toy_tree):
    toys.append(event.limit)

min_toy = min(toys)
max_toy = max(toys)

htoy = rt.TH1F("","",options.bins,0.9*(min(min_toy,single_limit)),1.1*(max(max_toy,single_limit)))
for toy in toys:
    htoy.Fill(toy)

#Drawing
c = rt.TCanvas('mycv','mycv',5,30,W_ref,H_ref)
htoy.Draw()
pvalue = 1.0*len(np.where(np.sort(toys)>single_limit)[0])/len(toys)

xaxis = htoy.GetXaxis()
yaxis = htoy.GetYaxis()
yaxis.SetTitle("Toys")

arrow=rt.TArrow(single_limit,1,single_limit,htoy.GetMaximum()*0.3,0.03,"<|")
arrow.SetLineWidth(4)
arrow.SetLineColor(rt.TColor.GetColor(histFillColor[1]))
arrow.SetFillColor(rt.TColor.GetColor(histFillColor[1]))
arrow.Draw()
latex = rt.TLatex()
latex.SetTextColor(rt.kBlack)
latex.SetTextAlign(12)
latex.DrawLatex(min(min_toy,single_limit),htoy.GetMaximum()*0.95,"p-value: {}".format(pvalue))
latex.Draw("same")
c.Update()
c.Modified()


version = "{}_{}".format(options.dir.strip("/"),options.mode)

if not os.path.exists(os.path.join(options.plots,"GOF")):
    os.makedirs(os.path.join(options.plots,"GOF"))
else:
    print "WARNING: directory already exists. Will overwrite existing files..."    
c.SaveAs(os.path.join(options.plots,"GOF","{0}.pdf".format(version)))
