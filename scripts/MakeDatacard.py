import os, sys,stat
from optparse import OptionParser
from math import sqrt 
import json

import ROOT
ROOT.gROOT.SetBatch()
ROOT.PyConfig.IgnoreCommandLineOptions = True
import CombineHarvester.CombineTools.ch as ch

parser = OptionParser(usage="%prog [options]")
parser.add_option("-f","--file",dest="file", type="string", default="mjjj_bkgfrac0.2_Full.root", help="Name of the root file containing the histograms. [default: %default]")
parser.add_option("-w","--wversion",dest="wversion", type="string", default="", help="Kind of weights to use [''] [_high] [_low]. [default: %default]")
parser.add_option("-m","--mode",dest="mode", type="string", default="test", help="Run the code with the test file [test] or the full file [nominal]. [default: %default]")
parser.add_option("-b","--bkgfrac",dest="bkgfrac", type="float", default=0.2, help="Background level used for histogram generation. [default: %default]")
parser.add_option("-p","--poi",dest="poi", type="string", default="mjjj", help="Name of the variable used for the fit. [default: %default]")
parser.add_option("-d","--dir",dest="dir", type="string", default="../root", help="Path to stored root files. [default: %default]")
parser.add_option("-o","--out",dest="output_dir", type="string", default="../datacards", help="Path to output datacards. [default: %default]")
parser.add_option("-v","--verbose",dest="verbose", action="store_true", default=False, help="Make the output verbose. [default: %default]")
parser.add_option("-r","--regions",dest="regions", action="store_true", default=False, help="Create a datacard that expects more than 1 region for the fit. [default: %default]")
parser.add_option("--fitOnly",dest="fitOnly", action="store_true", default=False, help="Only write FitDiagnostics related lines. [default: %default]")
parser.add_option("--gofOnly",dest="gofOnly", action="store_true", default=False, help="Only write GOF related lines. [default: %default]")
parser.add_option("--fullGof",dest="fullGof", action="store_true", default=False, help="Also run other GOF tests [AD/KS]. [default: %default]")
parser.add_option("-c","--checkChannelOnly",dest="checkChannelOnly", action="store_true", default=False, help="Only run the channel compatibility test. [default: %default]")

(options, args) = parser.parse_args()

if options.checkChannelOnly == True:
    if options.regions == False:
        print("ERROR: Channel compatibility should be run on multiple regions datacard")
        sys.exit()

file_name = "{}{}_bkgfrac{}.root".format(options.poi,options.wversion,options.bkgfrac)
if options.mode =='test':
    file_name=file_name.replace(".root","_Test.root")
elif options.mode=="nominal":
    file_name=file_name.replace(".root","_Full.root")
else:
    print("ERROR: Mode not available, try [nominal] or [test]")
    sys.exit()

if options.regions:
    file_name=file_name.replace(".root","_regions.root")

ADDTEXT=file_name.replace(".root","")
DATACARD_NAME = 'datacard.txt'
SHAPES_NAME = 'shapes.root'

output_dir = os.path.join(options.output_dir,ADDTEXT)

if options.regions:
    cats = [
        (1, 'lowsys'),
        (2, 'highsys'),     
    ]
    PROCESS_SYNTAX = "$BIN/$PROCESS"
    SYSTEMATIC_SYNTAX= "$BIN/$PROCESS_$SYSTEMATIC"
    ADDTEXT+="_regions"
else:
    cats = [(1, 'merged')]
    PROCESS_SYNTAX = "$PROCESS"
    SYSTEMATIC_SYNTAX= "$PROCESS_$SYSTEMATIC"

cb = ch.CombineHarvester()
if options.verbose:
    cb.SetVerbosity(3)

cb.AddObservations(['*'], ['SysToys'], ['13TeV'], ['SL'], cats)
cb.AddProcesses(['*'], ['SysToys'], ['13TeV'], ['SL'], ['sig'], cats, True)
cb.AddProcesses(['*'], ['SysToys'], ['13TeV'], ['SL'], ['bkg'], cats, False)

cb.cp().signals().AddSyst(cb, 'sys', 'shape', ch.SystMap()(1.))
cb.cp().backgrounds().AddSyst(cb, 'bkg_norm', 'lnN', ch.SystMap()(1.025))

#Extract the histograms from root files
cb.cp().signals().ExtractShapes(
    os.path.join(options.dir,file_name),
    PROCESS_SYNTAX,
    SYSTEMATIC_SYNTAX)

cb.cp().backgrounds().ExtractShapes(
    os.path.join(options.dir,file_name),
    PROCESS_SYNTAX,
    SYSTEMATIC_SYNTAX)


if not os.path.exists(output_dir):
    os.makedirs(output_dir)


datacard = os.path.join(output_dir, DATACARD_NAME)
output_shapes = os.path.join(output_dir, SHAPES_NAME)

fout = ROOT.TFile.Open(output_shapes,'recreate')
cb.WriteDatacard(datacard,fout)

#Creating some .sh files to perform the fits automatically
def createScript(content, filename):
    initWorkSpace = """
#!/bin/bash
text2workspace.py {} -o workspace.root
RMIN=0.
RMAX=5.0
NPOINTS=50
export FIT_OPT=( --cminDefaultMinimizerStrategy 0 --robustFit 1 )
""".format(DATACARD_NAME)

    script_path = os.path.join(output_dir, filename)
    with open(script_path, 'w') as f:
        f.write(initWorkSpace)
        f.write(content)
    # make script executable
    st = os.stat(script_path)
    os.chmod(script_path, st.st_mode | stat.S_IEXEC)

fit_script = """
combine -M FitDiagnostics -d workspace.root --rMin $RMIN --rMax $RMAX --skipBOnlyFit --saveShapes --saveNormalizations --saveWithUncertainties --plots "${FIT_OPT[@]}" 2>&1 | tee fitDiag.log
# Postfit plots
PostFitShapesFromWorkspace -d datacard.txt -w workspace.root -o postfit_shapes.root -m 120 -f fitDiagnostics.root:fit_s --postfit --sampling --print 2>&1 | tee postFitRates.log

"""

gof_script = """
# Goodness of fit
combine -M GoodnessOfFit workspace.root --algo=saturated -n saturated 
combine -M GoodnessOfFit workspace.root --algo=saturated -t 1000 -n saturated_1000  -s 1234
combine -M GoodnessOfFit workspace.root --algo=KS -n KS 
combine -M GoodnessOfFit workspace.root --algo=KS -t 1000 -n KS_1000  -s 1234
combine -M GoodnessOfFit workspace.root --algo=AD -n AD 
combine -M GoodnessOfFit workspace.root --algo=AD -t 1000 -n AD_1000  -s 1234 

"""

gof_script_full = """
# Goodness of fit
combine -M GoodnessOfFit workspace.root --algo=KS -n KS 
combine -M GoodnessOfFit workspace.root --algo=KS -t 1000 -n KS_1000  -s 1234
combine -M GoodnessOfFit workspace.root --algo=AD -n AD 
combine -M GoodnessOfFit workspace.root --algo=AD -t 1000 -n AD_1000  -s 1234 

"""
channel_comp_script = """
# Channel compatibility
combine -M ChannelCompatibilityCheck workspace.root -n CH
combine -M ChannelCompatibilityCheck workspace.root -n CH_1000 -t 1000 -s 1234

"""


script = fit_script + gof_script
if options.fitOnly: script=fit_script
if options.gofOnly: script=gof_script

if options.fullGof:script+=gof_script_full
if options.checkChannelOnly:script=channel_comp_script

createScript(script, 'do_fit.sh')
