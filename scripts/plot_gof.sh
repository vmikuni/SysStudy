#!/bin/bash
#Run on all relevant options for Plot_GOF.py
FOLDER=$1
CURRDIR="$PWD"
for f in $FOLDER/*; do
    if [ -d "$f" ]; then
	python Plot_GOF.py -d $(basename $f) -m saturated	
	#python Plot_GOF.py -d $(basename $f) -m KS	
	#python Plot_GOF.py -d $(basename $f) -m AD	
    fi
done
