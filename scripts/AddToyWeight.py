import sys, os
import ROOT; ROOT.PyConfig.IgnoreCommandLineOptions = True
from array import array
from math import *
from optparse import OptionParser
#Add branches to an existing root file
ROOT.gROOT.SetBatch(False)

parser = OptionParser(usage="%prog [options]")
parser.add_option("-m","--mode",dest="mode", type="string", default="test", help="Run the code with the test file [test] or the full file [nominal].  [default: %default]")
parser.add_option("-f","--folder",dest="folder", type="string", default="../data", help="Path to stored root files.  [default: %default]")
parser.add_option("-t","--tree",dest="tree", type="string", default="tree", help="TTree name.  [default: %default]")

(options, args) = parser.parse_args()

if options.mode=="test":
    file_name = "TestEvents.root"
elif options.mode=="nominal":
    file_name = "AllEvents.root"
else:
    print("ERROR: Mode not available, try [nominal] or [test]")
    sys.exit()

rfile = ROOT.TFile.Open(os.path.join(options.folder,file_name),"update")
tree = rfile.Get(options.tree)

#Variables to be added to the TTreee

new_vars = [
    'w_pt',
    'w_pt_up',
    'w_pt_down',
    'w_pt_high_up',
    'w_pt_high_down',
    'w_pt_low_up',
    'w_pt_low_down',
]


def AddBranch(tree,names):
    ''' Add a branch to an existing TTree and return the arrays pointing to the new branches '''
    arrays = {}
    branches = {}    
    for name in names:
        arrays[name] = array('f',[-10])
        branches[name] = tree.Branch(name,arrays[name],"{0}/F".format(name))
    
    return arrays, branches

arrays,branches = AddBranch(tree,new_vars)



for e,event in enumerate(tree):
    if e%10000==0:print("Reading event {}".format(e))
    #Create a linear weight component, relative uncertainty also increases linearly
    w = 1+event.jet_pt[0]*2e-3

    high_var = 9e-6*event.jet_pt[0]**2
    low_var = 1e-6*event.jet_pt[0]**2
    central_var = 4e-6*event.jet_pt[0]**2 

    w_up = w + central_var
    w_highup = w + high_var
    w_lowup = w + low_var

    w_down = w- central_var
    w_highdown = w- high_var
    w_lowdown = w- low_var
    


    arrays['w_pt'][0] = w
    arrays['w_pt_up'][0] = w_up
    arrays['w_pt_down'][0] = w_down
    arrays['w_pt_high_up'][0] = w_highup
    arrays['w_pt_high_down'][0] = w_highdown
    arrays['w_pt_low_up'][0] = w_lowup
    arrays['w_pt_low_down'][0] = w_lowdown


    for var in new_vars:        
        branches[var].Fill()



tree.Write("",ROOT.TObject.kOverwrite)
tree.Print()
