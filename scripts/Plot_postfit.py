import os,sys
import ROOT as rt
import  tdrstyle
import CMS_lumi
import array
from Plotting_cfg import *
import numpy as np
from math import *
import numpy as np
from collections import OrderedDict
from optparse import OptionParser
rt.PyConfig.IgnoreCommandLineOptions = True

rt.gROOT.SetBatch(True)
rt.gStyle.SetOptStat(0)
tdrstyle.setTDRStyle()
rt.TH1.SetDefaultSumw2()
rt.TH2.SetDefaultSumw2()

parser = OptionParser(usage="%prog [options]")
parser.add_option("-p","--plots",dest="plots", type="string", default="../Plots", help="Path to store plots. [default: %default]")
parser.add_option("-f","--folder",dest="folder", type="string", default="../datacards", help="Base path for the folder with results. [default: %default]")
parser.add_option("-d","--dir",dest="dir", type="string", default="mjjj_bkgfrac0.2_Test", help="Directory of the folder with the postfitshapes. The directory is relative to the folder set by -f. [default: %default]")
parser.add_option("--poi",dest="poi", type="string", default="mjjj", help="Variable to create the distribution. [default: %default]")


parser.add_option("-s","--save",dest="save", action="store_true", default=True, help="Save the plots. [default: %default]")
parser.add_option("-c","--channel",dest="channel",type="string", default="merged", help="Folder name inside postfitshapes. [default: %default]")
parser.add_option("--pull",dest="pull",action="store_true", default=False, help="Plot the pulls instead of the ratio. [default: %default]")
parser.add_option("--plotData",dest="plotData",action="store_true", default=True, help="Plot the data with the simulation. [default: %default]")
parser.add_option("--prefit",dest="prefit",action="store_true", default=False, help="Plot the prefit distributions. [default: %default]")
parser.add_option("--logY",dest="logY",action="store_true", default=False, help="Plot in log scale. [default: %default]")

(options, args) = parser.parse_args()



saveplots = options.save
plotData = options.plotData
is_pull = options.pull
ch = options.channel
is_pref = options.prefit
logY = options.logY

if 'regions' in options.dir and ch=='merged':
    print("ERROR:Merged channel chosen on region splitted datacard")
    sys.exit()

if 'regions' not in options.dir and ch!='merged':
    print("ERROR:Only merged channel is available without region splitting. Changing the channel to merged")
    ch = 'merged'

mode = 'postfit'
if is_pref: mode = 'prefit'


if is_pull:
    RMIN = -2.0
    RMAX = 2.0


ch_text = {
    'merged': 'Single Category',
    'highsys': 'High systematics category',
    'lowsys': 'Low systematics category',

}



version = "{}_{}".format(options.dir.strip("/"),ch)
if is_pull:
    version += '_Pulls'


if not os.path.isfile(os.path.join(options.folder,options.dir,'postfit_shapes.root')):
    print('ERROR: Run FitDiagnostics first!')
    sys.exit()

rfile = rt.TFile.Open(os.path.join(options.folder,options.dir,'postfit_shapes.root'),"READ")


processlist =OrderedDict([
    ('bkg','Background'),
    ('sig','Signal'),
    ('data_obs','Data'),
])


hstack =  rt.THStack(ch,ch)
fit = {}
for iproc, proc in enumerate(processlist):
    fit[proc] = rfile.Get('{0}_{1}/{2}'.format(ch,mode,proc)).Clone(proc+mode)
    if 'data_obs' not in proc:
        fit[proc].SetFillColor(rt.TColor.GetColor(histFillColor[iproc]))
        fit[proc].SetLineColor(rt.kBlack)
        hstack.Add(fit[proc])
    else:
        fit[proc].SetMarkerColor(1)
        fit[proc].SetMarkerStyle(20)
        fit[proc].SetMarkerSize(1.0)




ordered_procs = ['ttbb','ttb_other','tt2b','ttlf','ttcc','QCD','small_bkg']

c = rt.TCanvas(options.poi,options.poi,5,30,W_ref,H_ref)

pad1 = rt.TPad("pad1", "pad1", 0.0, 0.2 if plotData else 0.0, 1, 1.0)
pad1.Draw()
pad1.cd()
SetupCanvas(pad1, logY)
hframe = fit['sig'].Clone('frame') #Create a frame for the plots
hframe.Reset()

    

hframe.SetAxisRange(1, hstack.GetMaximum()*FRMLOG if logY else hstack.GetMaximum()*FRML,"Y");
xAxis = hframe.GetXaxis()
xAxis.SetLabelSize(0.)
yAxis = hframe.GetYaxis()
yAxis.SetNdivisions(6,5,0)
yAxis.SetLabelSize(FLS)
yAxis.SetTitleSize(FTS)
yAxis.SetTitleOffset(FTO)
yAxis.SetMaxDigits(3)
yAxis.SetTitle("Events / bin")
hframe.Draw()
c.Update()
c.Modified()



hstack.Draw('histsame')


herr = rfile.Get('{0}_{1}/TotalProcs'.format(ch,mode))
herr_prefit = rfile.Get('{0}_prefit/TotalProcs'.format(ch))
herr.SetFillColor( rt.kBlack )
herr.SetMarkerStyle(0)
herr.SetFillStyle(3354)
if plotData:
    fit['data_obs'].Draw("histpsame")
    herr.Draw('e2same')

pad1.cd()
pad1.Update()
pad1.RedrawAxis()
frame = c.GetFrame()

latex = rt.TLatex()

legend =  rt.TPad("legend_0","legend_0",x0_l + xshiftm,y0_l + yshiftm,x1_l+xshiftp, y1_l + yshiftp )

legend.Draw()
legend.cd()

if plotData:
    gr_l =  rt.TGraphErrors(1, x_l, y_l, ex_l, ey_l)
    rt.gStyle.SetEndErrorSize(0)
    gr_l.SetMarkerSize(0.9)
    gr_l.Draw("0P")

latex.SetTextFont(42)
latex.SetTextAngle(0)
latex.SetTextColor(rt.kBlack)
latex.SetTextSize(legendsize)
latex.SetTextAlign(12)
yy_ = y_l[0]

if plotData:
    latex.DrawLatex(xx_+1.*bwx_,yy_,"Data")
nkey=0


for key in processlist:
    if 'data_obs' in key:continue
    box_ = rt.TBox()    
    SetupBox(box_,yy_,fit[key].GetFillColor())
    if nkey %2 == 0:
        xdist = xgap
    else:
        yy_ -= gap_
        xdist = 0
    box_.DrawBox( xx_-bwx_/2 - xdist, yy_-bwy_/2, xx_+bwx_/2 - xdist, yy_+bwy_/2 )
    box_.SetFillStyle(0)
    box_.DrawBox( xx_-bwx_/2 -xdist, yy_-bwy_/2, xx_+bwx_/2-xdist, yy_+bwy_/2 )
    latex.DrawLatex(xx_+1.*bwx_-xdist,yy_,processlist[key])
    nkey+=1


box_ = rt.TBox()
if nkey%2!=0:
    yy_ -= gap_
xdist = xgap
SetupBox(box_,yy_)
box_.SetFillStyle(3354)
box_.DrawBox( xx_-bwx_/2 - xdist, yy_-bwy_/2, xx_+bwx_/2- xdist, yy_+bwy_/2 )
latex.DrawLatex(xx_+1.*bwx_- xdist,yy_,'Total uncert')

latex.DrawLatex(xx_,yy_-gap_,ch_text[ch])
c.Update()


if plotData:
    rt.gStyle.SetHatchesLineWidth(1)
    c.cd()
    p1r = rt.TPad("p4","",0,0,1,0.26)

    p1r.SetRightMargin(P2RM)
    p1r.SetLeftMargin(P2LM)
    p1r.SetTopMargin(P2TM)
    p1r.SetBottomMargin(P2BM)
    p1r.SetTicks()
    p1r.Draw()
    p1r.cd()

    xmin = float(herr.GetXaxis().GetXmin())
    xmax = float(herr.GetXaxis().GetXmax())
    one = rt.TF1("one","1",xmin,xmax)
    if is_pull:
        one = rt.TF1("one","0",xmin,xmax)

    one.SetLineColor(1)
    one.SetLineStyle(2)
    one.SetLineWidth(1)

    nxbins = herr.GetNbinsX()
    hratio = fit['data_obs'].Clone()

    he = herr.Clone()
    #he.SetFillColor( 16 )
    he.SetFillStyle( 3354 )

    for b in range(nxbins):
        nbkg = herr.GetBinContent(b+1)        
        ebkg = herr.GetBinError(b+1)
        npref = herr_prefit.GetBinContent(b+1)            
        epref = herr_prefit.GetBinError(b+1)            
        ndata = fit['data_obs'].GetBinContent(b+1)
        edata = fit['data_obs'].GetBinError(b+1)
        #print nbkg/(ebkg**2)
        if is_pull:
            if is_pref:
                unc = ebkg
            else:
                ndata = npref
                unc = sqrt(epref**2-ebkg**2)
            r = (ndata -nbkg)/unc if ebkg >0 else 0
            rerr = 0
        else:
            r = ndata / nbkg if nbkg>0 else 0
            rerr = edata / nbkg if nbkg>0 else 0

        hratio.SetBinContent(b+1, r)
        hratio.SetBinError(b+1,rerr)

        he.SetBinContent(b+1, 1)
        he.SetBinError(b+1, ebkg/nbkg if nbkg>0 else 0 )

    
    hratio.GetYaxis().SetRangeUser(RMIN,RMAX)

    hratio.SetTitle("")

    hratio.GetXaxis().SetTitle('m_{jjj} [GeV]')
    hratio.GetXaxis().SetTitleSize(RTSX)
    hratio.GetXaxis().SetTitleOffset(RTOX)
    hratio.GetXaxis().SetLabelSize(RLSX)
    #hratio.GetXaxis().SetTickLength(0.09)

    hratio.GetXaxis().SetLabelOffset(0.02)

    hratio.GetYaxis().SetTitleSize(RTSY)
    hratio.GetYaxis().SetLabelSize(RLSY)
    hratio.GetYaxis().SetTitleOffset(RTOY)
    if is_pull:
        hratio.GetYaxis().SetTitle("Pull")
    else:
        hratio.GetYaxis().SetTitle("            Data / pred")
    hratio.GetYaxis().SetDecimals(1)
    hratio.GetYaxis().SetNdivisions(2,2,0) #was 402
    #hratio.GetXaxis().SetNdivisions(6,5,0)

    hratio.Draw("pe")
    #    setex2.Draw()
    if not is_pull:
        he.Draw("e2same")
    one.Draw("SAME")
    #turn off horizontal error bars
    #    setex1.Draw()
    hratio.Draw("PEsame")
    hratio.Draw("PE0X0same")
    hratio.Draw("sameaxis") #redraws the axes
    p1r.Update()

if saveplots:
    if not os.path.exists(os.path.join(options.plots,mode)):
        os.makedirs(os.path.join(options.plots,mode))
    else:
        print("WARNING: directory already exists. Will overwrite existing files...")
    c.SaveAs(os.path.join(options.plots,mode,"{}.pdf".format(version)))
