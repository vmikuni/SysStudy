#!/bin/bash
FOLDER=$1
CURRDIR="$PWD"
for f in $FOLDER/*; do
    if [ -d "$f" ]; then
	echo "Entering the folder $f"
	cd $CURRDIR/$f
	./do_fit.sh		
	cd $CURRDIR
    fi
done
