#!/bin/bash
OPTION=$1
#Run on all the possible options for MakeDatacard.py
python MakeDatacard.py $OPTION
python MakeDatacard.py  -w _high $OPTION
python MakeDatacard.py  -w _low $OPTION
python MakeDatacard.py  -m nominal $OPTION
python MakeDatacard.py  -m nominal -w _high $OPTION
python MakeDatacard.py  -m nominal -w _low $OPTION
#Create datacards with separated regions
python MakeDatacard.py  -r $OPTION
python MakeDatacard.py  -w _high -r $OPTION
python MakeDatacard.py  -w _low -r $OPTION
python MakeDatacard.py  -m nominal -r $OPTION
python MakeDatacard.py  -m nominal -w _high -r $OPTION
python MakeDatacard.py  -m nominal -w _low -r $OPTION
