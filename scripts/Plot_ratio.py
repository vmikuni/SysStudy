#Plotting.py
import os
import ROOT; ROOT.PyConfig.IgnoreCommandLineOptions = True
import  tdrstyle
#import CMS_lumi
import array
from Plotting_cfg import *
import numpy as np
from math import *
import sys
from optparse import OptionParser
ROOT.PyConfig.IgnoreCommandLineOptions = True

ROOT.gROOT.SetBatch(True)
tdrstyle.setTDRStyle()

parser = OptionParser(usage="%prog [options]")
parser.add_option("-m","--mode",dest="mode", type="string", default="test", help="Run the code with the test file [test] or the full file [nominal]. [default: %default]")
parser.add_option("-p","--plots",dest="plots", type="string", default="../Plots", help="Path to store plots. [default: %default]")
parser.add_option("-d","--data",dest="data", type="string", default="../data", help="Path to data. [default: %default]")
parser.add_option("-r","--root",dest="root", type="string", default="../root", help="Path to output root files. [default: %default]")
parser.add_option("-t","--tree",dest="tree", type="string", default="tree", help="TTree name. [default: %default]")
parser.add_option("-w","--wversion",dest="wversion", type="string", default='', help="Which weights to use. [default: %default]")
parser.add_option("-v","--verbose",dest="verbose", action="store_true", default=False, help="Run the verbose. [default: %default]")
parser.add_option("-s","--scale",dest="scale", type="long", default=3, help="Canvas scale. [default: %default]")
parser.add_option("--poi",dest="poi", type="string", default="mjjj", help="Variable to create the distribution. [default: %default]")
parser.add_option("--minh",dest="minh", type="long", default=150, help="Minimum for histogram distribution. [default: %default]")
parser.add_option("--maxh",dest="maxh", type="long", default=300, help="Minimum for histogram distribution. [default: %default]")
parser.add_option("--bins",dest="bins", type="int", default=20, help="Number of bins to fill the histogram. [default: %default]")
parser.add_option("--bkgfrac",dest="bkgfrac", type="float", default=0.2, help="Mock background fraction to be generated. [default: %default]")
parser.add_option("--cut",dest="cut", type="float", default=100, help="Cut used to to create a region on jetpt. [default: %default]")

(options, args) = parser.parse_args()



if options.mode=="test":
    file_name = "TestEvents.root"
    VERSION = '_Test'
elif options.mode=="nominal":
    file_name = "AllEvents.root"
    VERSION = '_Full'
else:
    print("ERROR: Mode not available, try [nominal] or [test]")
    sys.exit()


rfile = ROOT.TFile.Open(os.path.join(options.data,file_name),"read")
tree = rfile.Get(options.tree)
scale = options.scale

def MockBackground(entries,name,bins,minh=0,maxh=100):
    '''Create a mock 3rd degree polynomial. Parameters were derived to a fit to ttbar MC'''
    bkg_f = ROOT.TF1("pol","pol3",minh,maxh)
    bkg_f.SetParameter(0, 1)
    bkg_f.SetParameter(1, -77.4866)
    bkg_f.SetParameter(2, 0.796036)
    bkg_f.SetParameter(3, -0.00162247)
    h = ROOT.TH1F("bkg_{}".format(name),"bkg_{}".format(name),bins,minh,maxh)
    h.FillRandom("pol",entries)
    #h.Draw()
    return h



def Plotratio(nominal,hists,legtext=[],name='',options=''):
    '''Plot the data MC comparison. nominal is the data histogram while hists contain the MC and any variation'''


    if options == '':
        print('ERROR: Missing options')
        return

    ROOT.gStyle.SetLegendFont(42)
    c = ROOT.TCanvas('mycv','mycv',5,30,scale*W_ref,scale*H_ref)
    c.Divide(1,2)
    SetupCanvas(c, 0)
    #c.SetLogy()
    
    
    c.cd(1)
    xshift = -0.1
    yshift = -0.45
    legend = ROOT.TLegend(x0_l+xshift,y0_l+yshift,x1_l-0.1+xshift, y1_l-0.1+yshift)
    legend.SetTextSize(7)

    nominal.SetMarkerColor(1)
    nominal.SetMarkerStyle(20)
    nominal.SetMarkerSize(3.0)
    

    yAxis = nominal.GetYaxis()
    yAxis.SetNdivisions(6,5,0)
    yAxis.SetTitleOffset(1)
    yAxis.SetTitleSize(0.05)
    yAxis.SetTitle("Events")

    nominal.Draw('histpe')
    hratios = []
    for icolor, hist in enumerate(hists):    
        c.cd(1)
        hist.SetLineColor(ROOT.TColor.GetColor(histFillColor[icolor]))
        hist.SetLineWidth(3)

        if len(legtext)>0:
            legend.AddEntry(hist,legtext[icolor],'pl')
        else:
            legend.AddEntry(hist,hist.GetName(),'pl')

        hist.Draw("histsame")        
        legend.SetTextSize(0.03)
        legend.Draw('samex0')

        c.cd(2)
        hratios.append(hist.Clone('hratio'+hist.GetName()))
        hratios[-1].Divide(nominal)            
        hratios[-1].Draw("histsame")
      
        if 'jet_pt' in name:
            hratios[-1].GetXaxis().SetTitle("Leading jet p_{T} [GeV]")
        else:
            hratios[-1].GetXaxis().SetTitle("m_{jjj} [GeV]")
        hratios[-1].GetYaxis().SetTitle("Ratios")
        hratios[-1].GetYaxis().SetRangeUser(0.85,1.15) 

        #CMS_lumi.CMS_lumi(c, iPeriod, iPos)

    c.Update()
    c.Modified()
    c.RedrawAxis()

        
    

    
    one = ROOT.TF1("one","1",0,options.maxh)
    one.SetLineColor(1)
    one.SetLineStyle(2)
    one.SetLineWidth(1)
    one.Draw("same")
    
    
    if not os.path.exists(os.path.join(options.plots,"SYS")):
        os.makedirs(os.path.join(options.plots,"SYS"))
    else:
        print "WARNING: directory already exists. Will overwrite existing files..."
        

    
    c.SaveAs(os.path.join(options.plots,"SYS","{0}.pdf".format(name)))

    

cuts = {
    'jet_pt[0]>': {
        "value":options.cut,
        "name":"PtLarge",
 
    },
    'jet_pt[0]<':{
        "value":options.cut,
        "name": 'PtSmall',
    }

}


plots = {
    'jet_pt[0]':[20,50,200],
    'mjjj':[options.bins,options.minh,options.maxh]
}


weights = [
    'w_pt',
    'w_pt{}_down'.format(options.wversion),
    'w_pt{}_up'.format(options.wversion)]

hists = {}
MAXENTRIES = tree.GetEntries()


for plot in plots:
    #Create the histograms
    hists[plot] = {
        'none':ROOT.TH1F("none"+plot,"none"+plot,plots[plot][0],plots[plot][1],plots[plot][2])
    }
    hists[plot]['none'].Sumw2()
    for weight in weights:
        hists[plot][weight] = ROOT.TH1F(weight+plot,weight+plot,plots[plot][0],plots[plot][1],plots[plot][2])
        hists[plot][weight].Sumw2()
    for cut in cuts:
        hists[plot+cut] = {
            'none':ROOT.TH1F("none"+plot+cuts[cut]['name'],"none"+plot+cuts[cut]['name'],plots[plot][0],plots[plot][1],plots[plot][2])
        }
        hists[plot+cut]['none'].Sumw2()
        for weight in weights:
            hists[plot+cut][weight] = ROOT.TH1F(weight+plot+cuts[cut]['name'],weight+plot+cuts[cut]['name'],plots[plot][0],plots[plot][1],plots[plot][2])
            hists[plot+cut][weight].Sumw2()


    #Fill histograms
    
    if options.verbose:
        print ('nominal: ', "{}>>{},'','',{},{}".format(plot,'none'+plot,MAXENTRIES/2,MAXENTRIES/2))
        print ('weighted: ', "{}>>{},{},'',{},{}".format(plot,weight+plot,weight,MAXENTRIES/2,0))

    tree.Draw("{}>>{}".format(plot,'none'+plot),'','',MAXENTRIES/2,MAXENTRIES/2)

    for weight in weights:        
        tree.Draw("{}>>{}".format(plot,weight+plot),weight,'',MAXENTRIES/2,0)
    for cut in cuts:
        tree.Draw("{}>>{}".format(plot,"none"+plot+cuts[cut]['name']),'{}{}'.format(cut,cuts[cut]['value']),'',MAXENTRIES/2,MAXENTRIES/2)
        for weight in weights:
            tree.Draw("{}>>{}".format(plot,weight+plot+cuts[cut]['name']) ,"{}*({}{})".format(weight,cut,cuts[cut]['value']),'',MAXENTRIES/2,0)  
    if plot == options.poi:
        hists[plot]['bkg_data'] = MockBackground(int(hists[plot]['none'].Integral()*options.bkgfrac),plot+'data',options.bins,options.minh,options.maxh)
        hists[plot]['bkg_mc'] = MockBackground(int(hists[plot]['none'].Integral()*options.bkgfrac),plot+'mc',options.bins,options.minh,options.maxh)
        for cut in cuts:
            hists[plot+cut]['bkg_data'] = MockBackground(int(hists[plot+cut]['none'].Integral()*options.bkgfrac),plot+'data'+cut,options.bins,options.minh,options.maxh)
            hists[plot+cut]['bkg_mc'] = MockBackground(int(hists[plot+cut]['none'].Integral()*options.bkgfrac),plot+'mc'+cut,options.bins,options.minh,options.maxh)
    
                

    #Scale histograms to normalize the weight variations
    SUMNOM = hists[plot]['none'].Integral()
    SUMW = hists[plot]['w_pt'].Integral()
    for weight in weights:
        hists[plot][weight].Scale(SUMNOM/SUMW)
    for cut in cuts:
        for weight in weights:
            hists[plot+cut][weight].Scale(SUMNOM/SUMW)
            pass



for plot in plots:
    Plotratio(hists[plot]['none'],[hists[plot]['w_pt'],hists[plot]['w_pt{}_up'.format(options.wversion)],hists[plot]['w_pt{}_down'.format(options.wversion)]],['nominal','Up','Down'],name=plot+options.wversion,options=options)
    

    if plot == options.poi:        
        #Prepare output root files with histograms to use in combine        
        if not os.path.exists(options.root):
            os.makedirs(options.root)
            
        if options.verbose:
            print('{}{}_{}.root'.format(plot,options.wversion,options.bkgfrac))
        fout = ROOT.TFile.Open(os.path.join(options.root,'{}{}_bkgfrac{}{}.root'.format(plot,options.wversion,options.bkgfrac,VERSION)),"recreate")
        hists[plot]['none'].Add(hists[plot]['bkg_data'])
        hists[plot]['none'].SetName('data_obs')
        hists[plot]['bkg_mc'].SetName('bkg')
        hists[plot]['none'].Write()
        hists[plot]['bkg_mc'].Write()
        for weight in weights:
            name = 'sig'
            if 'up' in weight:name+='_sysUp'                
            elif 'down' in weight:name+='_sysDown'

            hists[plot][weight].SetName(name)
            hists[plot][weight].Write()
        fout.Close()
        #Create a file with histograms split into regions for the fit

        fout=ROOT.TFile.Open(os.path.join(options.root,'{}{}_bkgfrac{}{}_regions.root'.format(plot,options.wversion,options.bkgfrac,VERSION)),"recreate")

    for cut in cuts:                           
        Plotratio(hists[plot+cut]['none'],[hists[plot+cut]['w_pt'],hists[plot+cut]['w_pt{}_up'.format(options.wversion)],hists[plot+cut]['w_pt{}_down'.format(options.wversion)]],['nominal','Up','Down'],name=plot+cuts[cut]['name']+options.wversion,options=options)
   
        if plot == options.poi:            
            folder = 'highsys'
            if '<' in cut:
                folder = 'lowsys'
            fout.mkdir(folder)
            fout.cd(folder)
            
            hists[plot+cut]['none'].Add(hists[plot+cut]['bkg_data'])
            hists[plot+cut]['none'].SetName('data_obs')
            hists[plot+cut]['bkg_mc'].SetName('bkg')
            hists[plot+cut]['none'].Write()
            hists[plot+cut]['bkg_mc'].Write()
            for weight in weights:
                #hists[plot+cut][weight].Add(hists[plot+cut]['bkg_mc'])
                name = 'sig'
                if 'up' in weight:
                    name+='_sysUp'                
                elif 'down' in weight:
                    name+='_sysDown'

                hists[plot+cut][weight].SetName(name)
                hists[plot+cut][weight].Write()

            fout.cd('../')
    fout.Close()
