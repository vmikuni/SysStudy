# Installation

Install CMSSW first

```bash
cmsrel CMSSW_10_2_15_patch2
cd CMSSW_10_2_15_patch2/src/
cmsenv
```
Get the code

```bash
cd src
git clone https://gitlab.cern.ch/vmikuni/SysStudy.git
```

## Data location

```bash
/work/vmikuni/data_sys/nominal
```

to get your hands on the data easily just execute the setup.sh file
```bash
./setup.sh
```
2 files should be created under the data folder: ```TestEvents.root``` and ```AllEvents.root```, for testing and applications purposes.
If you want to start again with fresh root files just run ```setup.sh``` again.

## Adding toy weights to an existing root file

You can create weights for the toy study using the script ```AddToyWeight.py``` under scripts. Run with -h to see the available options

## Creating initial plots and root files with histograms

Execute the ```Plot_ratio.py``` script under scripts. Plots are stored in the folder ```Plots``` (or any other folder specified in the option -p) while the root files with the histograms are stored in the folder ```root``` (or also in any other folder specified with the option -r)

## Getting combine
Checking out Combinetools and CombineHarvester (documentation at: http://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/)
*Important: tier3 ui07 does not compile CMSSW properly, you have to use ui[01-03] to compile it

```bash
cd src
export SCRAM_ARCH=slc7_amd64_gcc700
git clone https://github.com/cms-analysis/HiggsAnalysis-CombinedLimit.git HiggsAnalysis/CombinedLimit
git clone https://github.com/cms-analysis/CombineHarvester.git CombineHarvester
cd HiggsAnalysis/CombinedLimit
git fetch origin
git checkout v8.0.1
scramv1 b clean; scramv1 b # always make a clean build
```
## Creating datacards
You can automatically create datacards to be used in combine with ```MakeDatacard.py```. The main arguments to be given are similar to the ones used in ```Plot_ratio.py```. Example:
```bash
python MakeDatacard.py -w [''/_high/_low] -m [test/nominal]
```
Irrespective to the method, running ```MakeDatacard.py``` creates the relevant datacard and root file under the directory ```datacards``` together with a .sh file. You can run any Combine method you would like directly on the datacard, or you can run some of the methods defined in the .sh file. To do that, just uncomment the method you want from the shell file and run.


If you want to run ```Plot_ratio.py``` and ```MakeDatacard.py``` for all possible combinations of weights and samples, you can do
```bash
./plot_ratio.sh
./makedatacard.sh
```
If you want to run ```do_fit.sh``` for all scripts under a particular folder you can do

```bash
./dofits.sh ../datacardd/ #or any other folder which the datacards were created at
```


## Plotting prefit and postfit distributions

After running ```FitDiagnostics``` and ```PostFitShapesFromWorkspace``` in combine, a root file is created with all the prefit and postfit histograms. You can directly open the root files and look at the contents, or you can plot using ```Plot_postfit.py```. Examples:
```bash

python Plot_postfit.py -d [subdirectory with postfit_shapes.root] --pref #for prefit
python Plot_postfit.py -d [subdirectory with postfit_shapes.root] #for postfit
 #note that the subdirectory is relative to the folder (set with -f) where the datacards are stored ../datacards by default
```
The code will create folders under ```Plots``` with the results.

## Plotting GOF

The ```do_fit.sh``` created will also have lines to run different GOF tests, if uncommented, they will produce the root files with the results for ```3000``` toys for 3 tests: ```KS,AD,``` and ```saturated```. To plot the results you can use ```Plot_GOF.py``` with the same arguments as ```Plot_postfit.py```.

```bash
python Plot_GOF.py -d [subdirectory with GOF root file] -m [saturated/KS/AD]
```
The resulting plots are stored at the folder ```GOF``` inside ```Plots```.
As always, you can do it automatically with

```bash
./plot_gof.sh ../datacards/

```
